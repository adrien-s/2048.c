CFLAGS ?= -std=c99
PREFIX = /usr/local
DESTDIR= $(PREFIX)/bin

.PHONY: all

all: 2048

2048: 2048.c
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -f 2048

install:
	install -dm755 $(DESTDIR)
	install -m755 2048 $(DESTDIR)/

uninstall:
	rm -f $(DESTDIR)/2048
